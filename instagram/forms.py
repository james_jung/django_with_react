from typing import re

import form as form
from django import forms
from django.core.checks import messages

from .models import Post

# class PostForm(forms.Form):
#     title = forms.CharField()
#     content = forms.CharField(widget=form.Textarea)


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        # fields = '__all__'
        fields = ['message', 'photo', 'is_public']

    def clean_message(self):
        message = self.cleaned_data.get('message')
        # if messages:
        #     message = re.sub(r'[a-zA-Z]+','',message)
        #     len(message) > 20
        return message