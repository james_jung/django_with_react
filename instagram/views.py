from django.core.checks import messages
from django.http import HttpResponse, HttpRequest, Http404
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, DetailView, ArchiveIndexView, CreateView, UpdateView, DeleteView
from django.shortcuts import render, get_object_or_404, redirect
from .models import Post
from .forms import PostForm


# def post_new(request):
#     if request.method == 'POST':
#         form = PostForm(request.POST, request.FILES)
#         if form.is_valid():
#             post = form.save(commit=False)
#             post.author = request.user
#             post.save()
#             return redirect(post)
#     else:
#         form = PostForm()
#     return render(request, 'instagram/post_form.html', {
#         'form': form,
#         'post': None,
#     })


class PostCreateView(CreateView):
    model = Post
    form_class = PostForm

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.objcts.author = self.request.user
        messages.Info(self.request, "Posting saved")
        return super().form_valid(form)


# def post_edit(request, pk):
#     post = get_object_or_404(Post, pk=pk)
#
#     if post.author != request.user:
#         messages.error(request, 'Only owner could modify')
#         return redirect(post)
#
#     if request.method == 'POST':
#         form = PostForm(request.POST, request.FILES, instance=post)
#         if form.is_valid():
#             post = form.save()
#             return redirect(post)
#     else:
#         form = PostForm(instance=post)
#     return render(request, 'instagram/post_form.html', {
#         'form': form,
#         'post': post,
#     })


class PostUpdateView(UpdateView):
    model = Post
    form_class = PostForm

    def form_valid(self, form):
        messages.Info(self.request, 'Posting modified')
        return super().form_valid(form)

# post_list = ListView.as_view(model=Post, paginate_by=2)

def post_list(request):
    qs = Post.objects.all()
    q = request.GET.get('q','')
    if q:
        qs = qs.filter(message__icontains=q)

    messages.Info(request, 'messages test')

    return render(request, 'instagram/post_list.html', {
        'post_list': qs,
        'q': q,
    })

    # request.POST
    # request.FILES

# def post_detail(request: HttpRequest, pk:int) -> HttpResponse:
#     response = HttpResponse()
#     response.write("Hellow World")
#     return response

# def post_detail(request: HttpRequest, pk:int) -> HttpResponse:
#     post = get_object_or_404(Post, pk=pk)
#     # try:
#     #     post = Post.objects.get(pk=pk)
#     # except Post.DoesNotExist:
#     #     raise Http404
#     return render(request, 'instagram/post_detail.html',{'post':post})

post_detail = DetailView.as_view(
    model=Post,
    )
    # queryset = Post.objects.filter(is_public=True))


# class PostDetailView(DetailView):
#     model = Post
#     # queryset = Post.objects.filter(is_public=True)
#
#     def get_queryset(self):
#         qs = super().get_queryset()
#         if not self.request.user.is_authenticated:
#             qs = qs.filter(is_public=True)
#         return qs

# def archives_year(request, year):
#     return HttpResponse(f"{year}년 archives")


post_archive = ArchiveIndexView.as_view(model=Post, date_field='created_at')


# def post_delete(request, pk):
#     post = get_object_or_404(Post, pk=pk)
#     if request.method == 'POST':
#         post.delete()
#         messages.Info(request, 'Posting deleted successfuly')
#         return redirect('instagram:post_list')
#     return render(request, 'instagram/post_confirm_delete.html',{'post':post})


class PostDeleteView(DeleteView):
    model = Post
    success_url = reverse_lazy('instagram:post_list')

    # def get_success_url(self):
    #     return reverse('instagram:post_list')

post_delete = PostDeleteView.as_view()